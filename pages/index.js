import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Yelow</title>
        <meta name="description" content="Inbound Outbound Call Center Solution" />
        {/* <link rel="icon" type="image/x-icon" href="/pacul.ico" /> */}
        <link rel="icon" type="image/x-icon" href="/pacul.ico" ></link>
      </Head>

      <div className={styles.bg}>
        <Image alt="" layout="fill" objectFit="contain" src={"/assets/call.png"} />
      </div>

      <div
      className={styles.containerInfo}
      
      >
        <div className={styles.titles}>
          <h1>Inbound Outbound</h1>
          <h1>Call Center</h1>
          <h1>Solution</h1>
        </div>

        <div className={styles.subTitle} >
          <p>Professional solutions to facilitate contact with your client.</p>
          <p>We have been in the industry for over 19 years.</p>
        </div>

        <div className={styles.linkDownload}>
         <Link href="https://play.google.com/" passHref>
         <a target="_blank" rel="noreferrer">
         <Image
         alt=""
            src={"/assets/gplay.png"}
            width={288}
            height={86}
            objectFit="contain"
          /></a></Link>
           <Link href="https://www.apple.com/id/app-store/" passHref>
           <a target="_blank" rel="noreferrer">
          <Image
           alt=""
            src={"/assets/appstore.png"}
            width={288}
            height={86}
            objectFit="contain"
          />
          </a>
              </Link>
        </div>
      </div>
    </div>
  );
}
